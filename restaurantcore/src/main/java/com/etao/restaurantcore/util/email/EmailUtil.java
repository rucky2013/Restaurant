package com.etao.restaurantcore.util.email;



import com.etao.restaurantcore.util.ConfigUtil;

/**
 * Created by mike on 2015/9/22.
 */
public final class EmailUtil {
   static ConfigUtil configUtil = ConfigUtil.getInstance();

    public static void send(String subject,String content){
        if(!configUtil.isOnline()){
            subject=String.format("%s-%s",subject,"ecb测试环境");
        }
        else {
            subject=String.format("%s-%s",subject,"ecb线上环境");
        }

        Email email = new Email();
        email.setMailHost(configUtil.getMailHost());
        email.setMailTransportProtocol("smtp");
        email.setUserName(configUtil.getMailUserName());
        email.setPassword(configUtil.getMailPassword());
        email.setFrom(configUtil.getMailFrom());
        email.setTo(configUtil.getMailTo().split(";"));
        email.setSubject(subject);
        email.setConent(content);

        try {
            email.send();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
