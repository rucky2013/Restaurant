package com.etao.restaurantcore.util.email;



import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

/**
 * Created by mike on 2015/9/22.
 */
public class Email {
    private String MailHost;
    private String MailTransportProtocol;
    private String UserName;
    private String Password;

    private String From;
    private String[] To;
    private String[] Cc;
    private String[] Bcc;

    private String Subject;
    private String Conent;

    private String[] Attach;

    public String getMailHost() {
        return MailHost;
    }

    public void setMailHost(String mailHost) {
        MailHost = mailHost;
    }

    public String getMailTransportProtocol() {
        return MailTransportProtocol;
    }

    public void setMailTransportProtocol(String mailTransportProtocol) {
        MailTransportProtocol = mailTransportProtocol;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    public String[] getTo() {
        return To;
    }

    public void setTo(String[] to) {
        To = to;
    }

    public String[] getCc() {
        return Cc;
    }

    public void setCc(String[] cc) {
        Cc = cc;
    }

    public String[] getBcc() {
        return Bcc;
    }

    public void setBcc(String[] bcc) {
        Bcc = bcc;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getConent() {
        return Conent;
    }

    public void setConent(String conent) {
        Conent = conent;
    }

    public String[] getAttach() {
        return Attach;
    }

    public void setAttach(String[] attach) {
        Attach = attach;
    }

    public void send() throws MessagingException {
        Properties prop = new Properties();
        prop.setProperty("mail.host", MailHost);
        prop.setProperty("mail.transport.protocol", MailTransportProtocol);
        prop.setProperty("mail.smtp.auth", "true");
        //使用JavaMail发送邮件
        //1、创建session
        Session session = Session.getInstance(prop);
        //开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
        //        session.setDebug(true);
        //2、通过session得到transport对象
        Transport ts = session.getTransport();
        ts.connect(MailHost, UserName, Password);
        //4、创建邮件
        Message message = createAttachMail(session);
        //5、发送邮件
        ts.sendMessage(message, message.getAllRecipients());
        ts.close();
    }

    private Message createAttachMail(Session session) throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        //设置邮件的基本信息
        //邮件标题
        message.setSubject(Subject);
        //发件人
        message.setFrom(new InternetAddress(From));
        //收件人
        //        message.setRecipient(Message.RecipientType.TO, new InternetAddress("test@sina.cn"));
        if (To != null && To.length > 0) {
            Address[] address = new Address[To.length];
            for (int i = 0; i < To.length; i++) {
                address[i] = new InternetAddress(To[i]);
            }
            message.setRecipients(Message.RecipientType.TO, address);
        }
        //抄送
        if (Cc != null && Cc.length > 0) {
            Address[] address = new Address[Cc.length];
            for (int i = 0; i < Cc.length; i++) {
                address[i] = new InternetAddress(Cc[i]);
            }
            message.setRecipients(Message.RecipientType.CC, address);
        }
        //密抄
        if (Bcc != null && Bcc.length > 0) {
            Address[] address = new Address[Bcc.length];
            for (int i = 0; i < Bcc.length; i++) {
                address[i] = new InternetAddress(Bcc[i]);
            }
            message.setRecipients(Message.RecipientType.BCC, address);
        }
        //创建容器描述数据关系
        MimeMultipart mp = new MimeMultipart();
        mp.setSubType("mixed");

        //创建邮件正文，为了避免邮件正文中文乱码问题，需要使用charset=UTF-8指明字符编码
        MimeBodyPart text = new MimeBodyPart();
        text.setContent(Conent, "text/html;charset=UTF-8");
        mp.addBodyPart(text);

        //创建邮件附件
        MimeBodyPart attach = new MimeBodyPart();
        if (Attach != null && Attach.length > 0) {
            for (int i = 0; i < Attach.length; i++) {
                DataHandler dh = new DataHandler(new FileDataSource(Attach[i]));
                attach.setDataHandler(dh);
                attach.setFileName(dh.getName());
                mp.addBodyPart(attach);
            }
        }

        message.setContent(mp);
        message.saveChanges();

        return message;
    }
}
