package com.etao.restaurantcore.util;



import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by mike on 2015/12/16.
 */
@Component
public final class ConfigUtil {
    private static ConfigUtil instance;
    public static ConfigUtil getInstance() {
        return instance;
    }
    public ConfigUtil() {
        instance = this;
    }

    @Value("${ecb.version}")
    private String EcbVersion;
    @Value("${ecb.js.version}")
    private String JsVersion;
    @Value("${ecb.css.version}")
    private String CssVersion;
    @Value("${ecb.isonline}")
    private boolean Online;
    @Value("${ecb.host}")
    private String EcbHost;
    @Value("${ecb.root.path}")
    private String EcbRootPath;
    /**
     * 下载目录
     */
    @Value("${ecb.upload.path}")
    public String EcbUploadPath;
    //============邮件配置信息==============
    @Value("${ecb.mail.mailhost}")
    private String MailHost;
    @Value("${ecb.mail.username}")
    private String MailUserName;
    @Value("${ecb.mail.password}")
    private String MailPassword;
    @Value("${ecb.mail.from}")
    private String MailFrom;
    @Value("${ecb.mail.to}")
    private String MailTo;
    @Value("${ecb.erp.sign}")
    private String ErpSign;
    //==============api接口=========================
    @Value("${ecb.erp.host}")
    private String ErpUrl;
    //--------提现管理
    @Value("${ecb.cash.selectcount}")
    private String CashSelectCount;
    @Value("${ecb.cash.findcash}")
    private String CashFindcash;
    @Value("${ecb.cash.cashtodk}")
    private String Cashcashtodk;
    @Value("${ecb.cash.cashok}")
    private String Cashcashok;
    @Value("${ecb.cash.cashfail}")
    private String Cashcashfail;
    @Value("${ecb.cash.findlogbycashid}")
    private String Cashfindlogbycashid;
    @Value("${ecb.cash.updatetransfernumber}")
    private String CashUpdateTransferNumber;
    @Value("${ecb.cash.updatecashbatch}")
    private String CashUpdateCashBatch;
    //--------账户管理
    @Value("${ecb.account.findaccount}")
    private String AccountFindaccount;
    @Value("${ecb.account.editaccount}")
    private String AccountEditaccount;
    @Value("${ecb.account.resetpassword}")
    private String AccountResetpassword;
    //---------门店管理
    @Value("${ecb.restaurant.getstastics}")
    private String RestaurantGetstastics;
    @Value("${ecb.restaurant.gettrend}")
    private String RestaurantGettrend;
    @Value("${ecb.restaurant.getrestaurantbypage}")
    private String RestaurantGetRestaurantBypage;
    @Value("${ecb.restaurant.editrestaurant}")
    private String RestaurantEditRestaurant;
    //---------订单管理
    @Value("${ecb.order.faceorderquerybypage}")
    private String Faceorderquerybypage;
    @Value("${ecb.order.foodorderquerybypage}")
    private String Foodorderquerybypage;
    @Value("${ecb.order.paymentmethodpath}")
    private String PaymentMethodPath;
    @Value("${ecb.order.orderdetailpath}")
    private String OrderDetailPath;
    /**
     * 订单统计趋势和统计接口地址
     */
    @Value("${ecb.order.orderStastistic}")
    private String orderStastistic;
    /**
     * 订单统计 当面付单数和金额  点餐单数和金额 和累计点数和金额接口地址
     */
    @Value("${ecb.order.orderTotal}")
    private String orderTotal;
    //---------营收统计
    @Value("${ecb.revenue.revenuestatistic}")
    private String revenuestatistic;
    @Value("${ecb.revenue.revenueincresedtrend}")
    private String revenueincresedtrend;

    //================用户登录配置信息================
    @Value("${ecb.login.limitDatetime}")
    private String LimitDatetime;
    @Value("${ecb.login.limitTime}")
    private String LimitTime;
    /**
     * 上传提现回执分隔符
     */
    @Value("${ecb.cash.split}")
    private String CashSplit;

    //================图片上传配置信息================
    /**
     * 图片上传服务器域名
     */
    @Value("${ecb.restaurant.logoPath}")
    private String logoPath;
    /**
     * 上传文件夹
     */
    @Value("${ecb.restaurant.folder}")
    private String folder;
    /**
     * 空间名称
     */
    @Value("${ecb.restaurant.upyun_space}")
    private String upyun_space;
    /**
     * 操作员名称
     */
    @Value("${ecb.restaurant.upyun_username}")
    private String upyun_username;
    /**
     * 操作员密码
     */
    @Value("${ecb.restaurant.upyun_password}")
    private String upyun_password;
    /**
     * 图片大小 40kb
     */
    @Value("${ecb.restaurant.maxFileSize}")
    private String maxFileSize;
    /**
     * 图片尺寸 长宽
     */
    @Value("${ecb.restaurant.maxFileWidth}")
    private String maxFileWidth;
    @Value("${ecb.restaurant.maxFileHeight}")
    private String maxFileHeight;

    public String getFindCommissionById() {
        return findCommissionById;
    }

    public void setFindCommissionById(String findCommissionById) {
        this.findCommissionById = findCommissionById;
    }

    public String getQueryCommissionByPage() {
        return queryCommissionByPage;
    }

    public void setQueryCommissionByPage(String queryCommissionByPage) {
        this.queryCommissionByPage = queryCommissionByPage;
    }

    public String getAddCommission() {
        return addCommission;
    }

    public void setAddCommission(String addCommission) {
        this.addCommission = addCommission;
    }

    public String getUpdateCommission() {
        return updateCommission;
    }

    public void setUpdateCommission(String updateCommission) {
        this.updateCommission = updateCommission;
    }

    public String getQueryOperateByPage() {
        return queryOperateByPage;
    }

    public void setQueryOperateByPage(String queryOperateByPage) {
        this.queryOperateByPage = queryOperateByPage;
    }

    /**
     * 体现管理
     * @return
     */
    @Value("${ecb.commission.findbyid}")
    private String findCommissionById;

    @Value("${ecb.commission.commissionquerybypage}")
    private String queryCommissionByPage;

    @Value("${ecb.commission.addcommission}")
    private String addCommission;

    @Value("${ecb.commission.updatecommission}")
    private String updateCommission;

    @Value("${ecb.commission.memoquerybypage}")
    private String queryOperateByPage;


    public String getMaxFileSize() {
        return maxFileSize;
    }

    public String getOrderStastistic() {
        return orderStastistic;
    }

    public void setOrderStastistic(String orderStastistic) {
        this.orderStastistic = orderStastistic;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    public void setMaxFileSize(String maxFileSize) {
        this.maxFileSize = maxFileSize;
    }

    public String getMaxFileWidth() {
        return maxFileWidth;
    }

    public void setMaxFileWidth(String maxFileWidth) {
        this.maxFileWidth = maxFileWidth;
    }

    public String getMaxFileHeight() {
        return maxFileHeight;
    }

    public void setMaxFileHeight(String maxFileHeight) {
        this.maxFileHeight = maxFileHeight;
    }

    public String getUpyun_space() {
        return upyun_space;
    }

    public void setUpyun_space(String upyun_space) {
        this.upyun_space = upyun_space;
    }

    public String getUpyun_username() {
        return upyun_username;
    }

    public void setUpyun_username(String upyun_username) {
        this.upyun_username = upyun_username;
    }

    public String getUpyun_password() {
        return upyun_password;
    }

    public void setUpyun_password(String upyun_password) {
        this.upyun_password = upyun_password;
    }

    public String getFolder() {
        return folder;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getEcbVersion() {
        return EcbVersion;
    }

    public void setEcbVersion(String ecbVersion) {
        EcbVersion = ecbVersion;
    }

    public String getJsVersion() {
        return JsVersion;
    }

    public void setJsVersion(String jsVersion) {
        JsVersion = jsVersion;
    }

    public String getCssVersion() {
        return CssVersion;
    }

    public void setCssVersion(String cssVersion) {
        CssVersion = cssVersion;
    }

    public static void setInstance(ConfigUtil instance) {
        ConfigUtil.instance = instance;
    }

    public boolean isOnline() {
        return Online;
    }

    public void setOnline(boolean online) {
        Online = online;
    }

    public String getMailHost() {
        return MailHost;
    }

    public void setMailHost(String mailHost) {
        MailHost = mailHost;
    }

    public String getMailUserName() {
        return MailUserName;
    }

    public void setMailUserName(String mailUserName) {
        MailUserName = mailUserName;
    }

    public String getMailPassword() {
        return MailPassword;
    }

    public void setMailPassword(String mailPassword) {
        MailPassword = mailPassword;
    }

    public String getMailFrom() {
        return MailFrom;
    }

    public void setMailFrom(String mailFrom) {
        MailFrom = mailFrom;
    }

    public String getMailTo() {
        return MailTo;
    }

    public void setMailTo(String mailTo) {
        MailTo = mailTo;
    }

    public String getLimitDatetime() {
        return LimitDatetime;
    }

    public void setLimitDatetime(String limitDatetime) {
        LimitDatetime = limitDatetime;
    }

    public String getLimitTime() {
        return LimitTime;
    }

    public void setLimitTime(String limitTime) {
        LimitTime = limitTime;
    }

    public String getErpUrl() {
        return ErpUrl;
    }

    public void setErpUrl(String erpUrl) {
        ErpUrl = erpUrl;
    }

    public String getCashSelectCount() {
        return CashSelectCount;
    }

    public void setCashSelectCount(String cashSelectCount) {
        CashSelectCount = cashSelectCount;
    }

    public String getCashFindcash() {
        return CashFindcash;
    }

    public void setCashFindcash(String cashFindcash) {
        CashFindcash = cashFindcash;
    }

    public String getCashcashtodk() {
        return Cashcashtodk;
    }

    public void setCashcashtodk(String cashcashtodk) {
        Cashcashtodk = cashcashtodk;
    }

    public String getCashcashok() {
        return Cashcashok;
    }

    public void setCashcashok(String cashcashok) {
        Cashcashok = cashcashok;
    }

    public String getCashcashfail() {
        return Cashcashfail;
    }

    public void setCashcashfail(String cashcashfail) {
        Cashcashfail = cashcashfail;
    }

    public String getCashfindlogbycashid() {
        return Cashfindlogbycashid;
    }

    public void setCashfindlogbycashid(String cashfindlogbycashid) {
        Cashfindlogbycashid = cashfindlogbycashid;
    }

    public String getCashUpdateTransferNumber() {
        return CashUpdateTransferNumber;
    }

    public void setCashUpdateTransferNumber(String cashUpdateTransferNumber) {
        CashUpdateTransferNumber = cashUpdateTransferNumber;
    }

    public String getCashUpdateCashBatch() {
        return CashUpdateCashBatch;
    }

    public void setCashUpdateCashBatch(String cashUpdateCashBatch) {
        CashUpdateCashBatch = cashUpdateCashBatch;
    }

    public String getAccountFindaccount() {
        return AccountFindaccount;
    }

    public void setAccountFindaccount(String accountFindaccount) {
        AccountFindaccount = accountFindaccount;
    }

    public String getAccountEditaccount() {
        return AccountEditaccount;
    }

    public void setAccountEditaccount(String accountEditaccount) {
        AccountEditaccount = accountEditaccount;
    }

    public String getAccountResetpassword() {
        return AccountResetpassword;
    }

    public void setAccountResetpassword(String accountResetpassword) {
        AccountResetpassword = accountResetpassword;
    }

    public String getRestaurantGetstastics() {
        return RestaurantGetstastics;
    }

    public void setRestaurantGetstastics(String restaurantGetstastics) {
        RestaurantGetstastics = restaurantGetstastics;
    }

    public String getRestaurantGettrend() {
        return RestaurantGettrend;
    }

    public void setRestaurantGettrend(String restaurantGettrend) {
        RestaurantGettrend = restaurantGettrend;
    }

    public String getRestaurantGetRestaurantBypage() {
        return RestaurantGetRestaurantBypage;
    }

    public void setRestaurantGetRestaurantBypage(String restaurantGetRestaurantBypage) {
        RestaurantGetRestaurantBypage = restaurantGetRestaurantBypage;
    }

    public String getRestaurantEditRestaurant() {
        return RestaurantEditRestaurant;
    }

    public void setRestaurantEditRestaurant(String restaurantEditRestaurant) {
        RestaurantEditRestaurant = restaurantEditRestaurant;
    }

    public String getErpSign() {
        return ErpSign;
    }

    public void setErpSign(String erpSign) {
        ErpSign = erpSign;
    }

    public String getCashSplit() {
        return CashSplit;
    }

    public void setCashSplit(String cashSplit) {
        CashSplit = cashSplit;
    }

    public String getEcbHost() {
        return EcbHost;
    }

    public void setEcbHost(String ecbHost) {
        EcbHost = ecbHost;
    }

    public String getEcbUploadPath() {
        return EcbUploadPath;
    }

    public void setEcbUploadPath(String ecbUploadPath) {
        EcbUploadPath = ecbUploadPath;
    }

    public String getEcbRootPath() {
        return EcbRootPath;
    }

    public void setEcbRootPath(String ecbRootPath) {
        EcbRootPath = ecbRootPath;
    }

    public String getFaceorderquerybypage() {
        return Faceorderquerybypage;
    }

    public void setFaceorderquerybypage(String faceorderquerybypage) {
        Faceorderquerybypage = faceorderquerybypage;
    }

    public String getFoodorderquerybypage() {
        return Foodorderquerybypage;
    }

    public void setFoodorderquerybypage(String foodorderquerybypage) {
        Foodorderquerybypage = foodorderquerybypage;
    }

    public String getPaymentMethod() {
        return PaymentMethodPath;
    }

    public String getOrderDetail() {
        return OrderDetailPath;
    }

    public String getPaymentMethodPath() {
        return PaymentMethodPath;
    }

    public void setPaymentMethodPath(String paymentMethodPath) {
        PaymentMethodPath = paymentMethodPath;
    }

    public String getOrderDetailPath() {
        return OrderDetailPath;
    }

    public void setOrderDetailPath(String orderDetailPath) {
        OrderDetailPath = orderDetailPath;
    }

    public String getRevenuestatistic() {
        return revenuestatistic;
    }

    public void setRevenuestatistic(String revenuestatistic) {
        this.revenuestatistic = revenuestatistic;
    }

    public String getRevenueincresedtrend() {
        return revenueincresedtrend;
    }

    public void setRevenueincresedtrend(String revenueincresedtrend) {
        this.revenueincresedtrend = revenueincresedtrend;
    }
}