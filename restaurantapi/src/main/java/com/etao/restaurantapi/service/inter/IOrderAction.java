package com.etao.restaurantapi.service.inter;

import java.text.ParseException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.etao.restaurantentity.common.Response;
import com.etao.restaurantentity.req.TangShiOrderReq;
import com.etao.restaurantentity.resp.SaveTangShiOrderResp;
import com.etao.restaurantentity.resp.TangShiOrderBaseModel;
import com.etao.restaurantentity.servicedomain.Dine;
import com.etao.restaurantentity.servicedomain.OrderDevice;

@Path("/Orders")
@Produces("application/json; charset=utf-8")//当前类的所有方法都返回json格式的数据
public interface IOrderAction{
	
	@POST
	@Consumes({"application/json; charset=utf-8","application/x-www-form-urlencoded"})
	@Path(value="/SaveTangShiOrder")
	//public Response<SaveTangShiOrderResp> SaveTangShiOrder(TangShiOrderReq tangShiOrder) throws ParseException, Exception;
	public Response<SaveTangShiOrderResp> SaveTangShiOrder(MultivaluedMap<String, String> params) throws ParseException, Exception;
	
	@GET
	@Path(value="/SavaGetOrderDeviceCount")
	public Response<Integer> selectDine(@QueryParam("orderNumber") String orderNumber,@QueryParam("deviceNumber") String deviceNumber);
	
	@GET
	@Path(value="/GetOrderWithOtherPlatform")
	public Response<TangShiOrderBaseModel> getOrderWithOtherPlatform(@QueryParam("id") String id,@QueryParam("orderType") int orderType,@QueryParam("orderSourceType") int orderSourceType);
}