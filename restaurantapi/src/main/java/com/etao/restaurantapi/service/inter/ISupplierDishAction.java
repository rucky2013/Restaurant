package com.etao.restaurantapi.service.inter;





import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.etao.restaurantentity.common.ListResponse;
import com.etao.restaurantentity.resp.SupplierDishResp;
import com.etao.restaurantentity.servicedomain.SupplierDish;




@Path("/Supplier")
@Produces("application/json; charset=utf-8")//当前类的所有方法都返回json格式的数据
public interface ISupplierDishAction{
	
	@GET
	@Path(value="/Menu")
	public ListResponse<SupplierDishResp> selectSupplierDishMenu(@QueryParam("id") int id,@QueryParam("supplierMenuCategoryTypeId") int supplierMenuCategoryTypeId);
}