package com.etao.restaurantapi.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aspectj.weaver.ast.Var;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etao.restaurantapi.service.inter.ISupplierDishAction;
import com.etao.restaurantcore.util.JsonUtil;
import com.etao.restaurantentity.common.ListResponse;
import com.etao.restaurantentity.resp.SupplierDishModel;
import com.etao.restaurantentity.resp.SupplierDishResp;
import com.etao.restaurantentity.servicedomain.SupplierDish;
import com.etao.restaurantservice.service.inter.ISupplierDishService;

@Service
public class SupplierDishAction implements ISupplierDishAction {
	private static Logger logger = LoggerFactory.getLogger("com.etao.restaurantapi");
	@Autowired
	private ISupplierDishService supplierDishService;

	public ListResponse<SupplierDishResp> selectSupplierDishMenu(int id, int supplierMenuCategoryTypeId) {
		logger.info(String.format("请求URL:%s 请求方式:%s 请求参数:%s", "/Api/Supplier/Menu","Get|application/x-www-form-urlencoded","?id="+id+"&supplierMenuCategoryTypeId="+supplierMenuCategoryTypeId ));
		SupplierDish supplierDish = new SupplierDish();
		supplierDish.setSupplierID(id);
		supplierDish.setSupplierMenuCategoryTypeId(supplierMenuCategoryTypeId);

		List<SupplierDish> supplierDishs = supplierDishService.selectSupplierDishMenu(supplierDish);
		ListResponse<SupplierDishResp> resp = new ListResponse<SupplierDishResp>();

		Map<String, SupplierDishResp> resultMap = new HashMap<String, SupplierDishResp>();
		List<SupplierDishResp> list = new ArrayList<SupplierDishResp>();
		for (SupplierDish sd : supplierDishs) {
			SupplierDishResp item = new SupplierDishResp();
			item.setCategoryName(sd.getCategoryName());
			item.setCategoryId(sd.getCategoryID());
			SupplierDishModel model = new SupplierDishModel();
			model.setImagePath("http://old.etaoshi.com/"+sd.getImagePath());
			model.setSupplierDishName(sd.getSupplierDishName());
			model.setPrice(sd.getPrice());
			model.setSupplierDishId(sd.getSupplierDishID());
			model.setSuppllierDishDescription(sd.getSuppllierDishDescription());
			model.setAverageRating(sd.getAverageRating());
			model.setIsCommission(sd.getIsCommission());
			model.setIsDiscount(sd.getIsDiscount());
			model.setRecipe(sd.getRecipe());
			model.setRecommended(sd.getRecommended());
			model.setPackagingFee(sd.getPackagingFee());
			model.setSupplierCatogryId(sd.getCategoryID());
			model.setSupplierMenuCategoryId(sd.getSupplierMenuCategoryId());
			item.setSupplierDishList(new ArrayList<SupplierDishModel>());
			item.getSupplierDishList().add(model);
			if (resultMap.containsKey(sd.getCategoryName())) {
				resultMap.get(sd.getCategoryName()).getSupplierDishList().add(model);
			} else {
				resultMap.put(sd.getCategoryName(), item);
			}
		}
		for (Map.Entry<String, SupplierDishResp> entry : resultMap.entrySet()) {
			list.add(entry.getValue());
		} 
		resp.setResult(list);
		return resp;
	}
}