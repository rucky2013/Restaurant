package com.etao.restaurantdao.dao.impl;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.etao.restaurantdao.base.BaseDao;
import com.etao.restaurantdao.dao.inter.IDineDao;
import com.etao.restaurantdao.dao.inter.IOrderDetailDao;
import com.etao.restaurantentity.common.page.PagerModel;
import com.etao.restaurantentity.servicedomain.Dine;
import com.etao.restaurantentity.servicedomain.OrderDetail;

@Repository
public class OrderDetailDaoImpl implements IOrderDetailDao {
	@Autowired
	private BaseDao dao;

	public PagerModel selectPageList(OrderDetail e) {
		return dao.selectPageList("front.account.selectPageList",
				"front.account.selectPageCount", e);
	}
	
	public OrderDetail selectById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List selectList(OrderDetail e) {
		return dao.selectList("front.account.selectList", e);
	}

	public OrderDetail selectOne(OrderDetail e) {
		return (OrderDetail) dao.selectOne("Dine.selectOne", e);
	}

	public int delete(OrderDetail e) {
		return dao.delete("front.account.delete", e);
	}

	public int update(OrderDetail e) {
		return dao.update("front.account.update", e);
	}

	public int deletes(int[] ids) {
		OrderDetail e = new OrderDetail();
		for (int i = 0; i < ids.length; i++) {
			e.setId(ids[i]);
			delete(e);
		}
		return 0;
	}

	public int insert(OrderDetail e) {
		return dao.insert("OrderDetail.insert", e);
	}

	public int deleteById(int id) {
		return dao.delete("front.account.deleteById", id);
	}

	public int selectCount(OrderDetail e) {
		return dao.getCount("front.account.selectCount", e);
	}
	
	public int insertList(List<OrderDetail> list)
	{
		return dao.insert("OrderDetail.insertList",list);
	}
}
