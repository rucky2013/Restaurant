package com.etao.restaurantdao.dao.inter;

import java.util.List;

import com.etao.restaurantdao.base.DaoManager;
import com.etao.restaurantentity.servicedomain.Dine;
import com.etao.restaurantentity.servicedomain.OrderDetail;
import com.etao.restaurantentity.servicedomain.SupplierDish;

public interface IOrderDetailDao extends DaoManager<OrderDetail> {
	
	
}
