package com.etao.restaurantdao.dao.impl;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.etao.restaurantdao.base.BaseDao;
import com.etao.restaurantdao.dao.inter.IDineDao;
import com.etao.restaurantentity.common.page.PagerModel;
import com.etao.restaurantentity.servicedomain.Dine;
import com.etao.restaurantentity.servicedomain.OrderDetail;

@Repository
public class DineDaoImpl implements IDineDao {
	@Autowired
	private BaseDao dao;

	public PagerModel selectPageList(Dine e) {
		return dao.selectPageList("front.account.selectPageList",
				"front.account.selectPageCount", e);
	}
	
	public Dine selectById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List selectList(Dine e) {
		return dao.selectList("front.account.selectList", e);
	}

	public Dine selectOne(Dine e) {
		return (Dine) dao.selectOne("Dine.selectOne", e);
	}

	public int delete(Dine e) {
		return dao.delete("front.account.delete", e);
	}

	public int update(Dine e) {
		return dao.update("front.account.update", e);
	}

	public int deletes(int[] ids) {
		Dine e = new Dine();
		for (int i = 0; i < ids.length; i++) {
			e.setId(ids[i]);
			delete(e);
		}
		return 0;
	}

	public int insert(Dine e) {
		return dao.insert("Dine.insert", e);
	}

	public int deleteById(int id) {
		return dao.delete("front.account.deleteById", id);
	}

	public int selectCount(Dine e) {
		return dao.getCount("front.account.selectCount", e);
	}
	
	public String GetDineNumber(Dine e){
		return ((Dine)dao.selectOne("Dine.GetDineNumber",e)).getOrderNumber();
	}

	public int insertList(List<Dine> list) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int updateIsPaid(Dine e) {
		return dao.update("Dine.updateIsPaid", e);
	}
}
