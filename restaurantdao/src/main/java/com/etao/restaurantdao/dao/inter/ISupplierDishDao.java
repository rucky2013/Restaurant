package com.etao.restaurantdao.dao.inter;

import java.util.List;

import com.etao.restaurantdao.base.DaoManager;
import com.etao.restaurantentity.servicedomain.SupplierDish;

public interface ISupplierDishDao extends DaoManager<SupplierDish> {

	List<SupplierDish> selectSupplierDishMenu(SupplierDish e);
}
