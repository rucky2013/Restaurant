package com.etao.restaurantdao.dao.inter;

import java.util.List;

import com.etao.restaurantdao.base.DaoManager;
import com.etao.restaurantentity.servicedomain.Dine;
import com.etao.restaurantentity.servicedomain.SupplierDish;

public interface IDineDao extends DaoManager<Dine> {
	String GetDineNumber(Dine e);
	int updateIsPaid(Dine e);
}
