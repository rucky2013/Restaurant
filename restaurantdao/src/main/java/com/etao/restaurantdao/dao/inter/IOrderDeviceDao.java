package com.etao.restaurantdao.dao.inter;

import com.etao.restaurantdao.base.DaoManager;
import com.etao.restaurantentity.servicedomain.OrderDevice;

public interface IOrderDeviceDao extends DaoManager<OrderDevice> {
	
}
