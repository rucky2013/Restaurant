package com.etao.restaurantdao.dao.impl;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.etao.restaurantdao.base.BaseDao;
import com.etao.restaurantdao.dao.inter.ISupplierDishDao;
import com.etao.restaurantentity.common.page.PagerModel;
import com.etao.restaurantentity.servicedomain.OrderDetail;
import com.etao.restaurantentity.servicedomain.SupplierDish;

import javax.annotation.Resource;

@Repository
public class SupplierDishDaoImpl implements ISupplierDishDao {
	@Autowired
	private BaseDao dao;

	public PagerModel selectPageList(SupplierDish e) {
		return dao.selectPageList("front.account.selectPageList",
				"front.account.selectPageCount", e);
	}
	
	public SupplierDish selectById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List selectList(SupplierDish e) {
		return dao.selectList("front.account.selectList", e);
	}

	public SupplierDish selectOne(SupplierDish e) {
		return (SupplierDish) dao.selectOne("front.account.selectOne", e);
	}

	public int delete(SupplierDish e) {
		return dao.delete("front.account.delete", e);
	}

	public int update(SupplierDish e) {
		return dao.update("front.account.update", e);
	}

	public int deletes(int[] ids) {
		SupplierDish e = new SupplierDish();
		for (int i = 0; i < ids.length; i++) {
			e.setId(ids[i]);
			delete(e);
		}
		return 0;
	}

	public int insert(SupplierDish e) {
		return dao.insert("front.account.insert", e);
	}

	public int deleteById(int id) {
		return dao.delete("front.account.deleteById", id);
	}

	public int selectCount(SupplierDish e) {
		return dao.getCount("front.account.selectCount", e);
	}
	
	
	@Override
	public List<SupplierDish> selectSupplierDishMenu(SupplierDish e)
	{
		return dao.selectList("SupplierDish.selectSupplierDishMenuForApp", e);
	}

	@Override
	public int insertList(List<SupplierDish> list) {
		// TODO Auto-generated method stub
		return 0;
	}
}
