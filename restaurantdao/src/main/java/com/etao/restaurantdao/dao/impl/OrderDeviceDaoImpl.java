package com.etao.restaurantdao.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.etao.restaurantdao.base.BaseDao;
import com.etao.restaurantdao.dao.inter.IOrderDeviceDao;
import com.etao.restaurantentity.common.page.PagerModel;
import com.etao.restaurantentity.servicedomain.OrderDetail;
import com.etao.restaurantentity.servicedomain.OrderDevice;

@Repository
public class OrderDeviceDaoImpl implements IOrderDeviceDao {
	@Autowired
	private BaseDao dao;

	public PagerModel selectPageList(OrderDevice e) {
//		return dao.selectPageList("front.account.selectPageList",
//				"front.account.selectPageCount", e);
		return null;
	}
	
	public OrderDevice selectById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List selectList(OrderDevice e) {
		//return dao.selectList("front.account.selectList", e);
		return null;
	}

	public OrderDevice selectOne(OrderDevice e) {
		return (OrderDevice) dao.selectOne("OrderDevice.SelectOne", e);
		
	}

	public int delete(OrderDevice e) {
		//return dao.delete("front.account.delete", e);
		return 0;
	}

	public int update(OrderDevice e) {
		return dao.update("front.account.update", e);
	}

	public int deletes(int[] ids) {
//		OrderDevice e = new OrderDevice();
//		for (int i = 0; i < ids.length; i++) {
//			e.setId(ids[i]);
//			delete(e);
//		}
//		return 0;
		return 0;
	}

	public int insert(OrderDevice e) {
		//return dao.insert("front.account.insert", e);
		return 0;
	}

	public int deleteById(int id) {
		//return dao.delete("front.account.deleteById", id);
		return 0;
	}

	public int selectCount(OrderDevice e) {
		//return dao.getCount("front.account.selectCount", e);
		return 0;
	}

	@Override
	public int insertList(List<OrderDevice> list) {
		// TODO Auto-generated method stub
		return 0;
	}
}
