package com.etao.restaurantentity.req;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SupplierDishItem{
	@JsonProperty(value = "SupplierDishId")
	private int supplierDishId;
	@JsonProperty(value = "SupplierDishName")
	private String supplierDishName;
	@JsonProperty(value = "DishCount")
	private int dishCount;
	@JsonProperty(value = "PackagingFee")
	private int packagingFee;
	@JsonProperty(value = "Price")
	private double price;
	@JsonProperty(value = "SupplierDishOptionGroup")
	private Object supplierDishOptionGroup;
	public int getSupplierDishId() {
		return supplierDishId;
	}
	public void setSupplierDishId(int supplierDishId) {
		this.supplierDishId = supplierDishId;
	}
	public int getDishCount() {
		return dishCount;
	}
	public void setDishCount(int dishCount) {
		this.dishCount = dishCount;
	}
	public int getPackagingFee() {
		return packagingFee;
	}
	public void setPackagingFee(int packagingFee) {
		this.packagingFee = packagingFee;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Object getSupplierDishOptionGroup() {
		return supplierDishOptionGroup;
	}
	public void setSupplierDishOptionGroup(Object supplierDishOptionGroup) {
		this.supplierDishOptionGroup = supplierDishOptionGroup;
	}
	public String getSupplierDishName() {
		return supplierDishName;
	}
	public void setSupplierDishName(String supplierDishName) {
		this.supplierDishName = supplierDishName;
	}
}