package com.etao.restaurantentity.req;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
public class TangShiOrderReq {
	
	@JsonProperty(value = "IsQr")
	private boolean isQr;
	@JsonProperty(value = "Source")
	private String source;
	@JsonProperty(value = "Path")
	private String path;
	@JsonProperty(value = "SupplierId")
	private int supplierId;
	@JsonProperty(value = "CustomerName")
	private String customerName;
	@JsonProperty(value = "CustomerSex")
	private String customerSex;
	@JsonProperty(value = "TableNo")
	private String tableNo;
	@JsonProperty(value = "Remark")
	private String remark;
	@JsonProperty(value = "TempOrderNumber")
	private String tempOrderNumber;
	@JsonProperty(value = "PayMentMethodId")
	private int payMentMethodId;
	@JsonProperty(value = "DeviceNumber")
	private String deviceNumber;
	@JsonProperty(value = "EquipmentNo")
	private String equipmentNo;
	@JsonProperty(value = "IsPack")
	private int isPack;
	@JsonProperty(value = "SupplierDishList")
	private String supplierDishList;
	
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerSex() {
		return customerSex;
	}

	public void setCustomerSex(String customerSex) {
		this.customerSex = customerSex;
	}

	public String getTableNo() {
		return tableNo;
	}

	public void setTableNo(String tableNo) {
		this.tableNo = tableNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTempOrderNumber() {
		return tempOrderNumber;
	}

	public void setTempOrderNumber(String tempOrderNumber) {
		this.tempOrderNumber = tempOrderNumber;
	}

	public int getPayMentMethodId() {
		return payMentMethodId;
	}

	public void setPayMentMethodId(int payMentMethodId) {
		this.payMentMethodId = payMentMethodId;
	}

	public String getDeviceNumber() {
		return deviceNumber;
	}

	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}

	public String getEquipmentNo() {
		return equipmentNo;
	}

	public void setEquipmentNo(String equipmentNo) {
		this.equipmentNo = equipmentNo;
	}

	public int getIsPack() {
		return isPack;
	}

	public void setIsPack(int isPack) {
		this.isPack = isPack;
	}

	public String getSupplierDishList() {
		return supplierDishList;
	}

	public void setSupplierDishList(String supplierDishList) {
		this.supplierDishList = supplierDishList;
	}

	public boolean getIsQr() {
		return isQr;
	}

	public void setIsQr(boolean isQr) {
		this.isQr = isQr;
	}
}