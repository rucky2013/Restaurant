package com.etao.restaurantentity.servicedomain;



import java.io.Serializable;



/**
 * 订单对象
 * @author jqsl2012@163.com
 *
 */
public class SupplierDish extends com.etao.restaurantentity.domain.SupplierDishEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public void clear() {
		super.clear();
		categoryName=null;
	}
	private int categoryID;
	private String categoryName;
	private int supplierMenuCategoryTypeId;
	private int supplierMenuCategoryId;
	private String imagePath;
	private String supplierDishOptionGroup;
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getSupplierMenuCategoryTypeId() {
		return supplierMenuCategoryTypeId;
	}
	public void setSupplierMenuCategoryTypeId(int supplierMenuCategoryTypeId) {
		this.supplierMenuCategoryTypeId = supplierMenuCategoryTypeId;
	}
	public int getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getSupplierDishOptionGroup() {
		return supplierDishOptionGroup;
	}
	public void setSupplierDishOptionGroup(String supplierDishOptionGroup) {
		this.supplierDishOptionGroup = supplierDishOptionGroup;
	}
	public int getSupplierMenuCategoryId() {
		return supplierMenuCategoryId;
	}
	public void setSupplierMenuCategoryId(int supplierMenuCategoryId) {
		this.supplierMenuCategoryId = supplierMenuCategoryId;
	}
}