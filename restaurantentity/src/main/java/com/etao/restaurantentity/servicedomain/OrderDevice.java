package com.etao.restaurantentity.servicedomain;

import java.io.Serializable;


/**
 * 堂食订单Service层实体
 * @author xupeng
 * @Time 2016-03-23
 */
public class OrderDevice extends com.etao.restaurantentity.domain.OrderDeviceEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public void clear() {
		super.clear();
	}
}
