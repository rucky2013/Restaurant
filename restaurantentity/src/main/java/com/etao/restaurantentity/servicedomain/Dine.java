package com.etao.restaurantentity.servicedomain;



import java.io.Serializable;
import java.util.List;

import com.etao.restaurantentity.req.SupplierDishItem;



/**
 * 订单对象
 * @author jqsl2012@163.com
 *
 */
public class Dine extends com.etao.restaurantentity.domain.DineEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public void clear() {
		super.clear();
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public List<SupplierDishItem> getSupplierDishList() {
		return supplierDishList;
	}
	public void setSupplierDishList(List<SupplierDishItem> supplierDishList) {
		this.supplierDishList = supplierDishList;
	}
	private String orderType;
	private List<SupplierDishItem> supplierDishList;
}