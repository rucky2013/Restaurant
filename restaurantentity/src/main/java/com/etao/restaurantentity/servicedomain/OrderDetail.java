package com.etao.restaurantentity.servicedomain;

import java.io.Serializable;


/**
 * 堂食订单明细Service层实体
 * @author xuyulong
 * @Time 2016-03-25
 */
public class OrderDetail extends com.etao.restaurantentity.domain.OrderDetailEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	public void clear() {
		super.clear();
	}
}
