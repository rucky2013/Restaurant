package com.etao.restaurantentity.servicedomain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EtsPaymentOrder  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String amount;
	
	
	private String body;
	
	
	private String orderId;
	
	
	private String authCode;
	
	
	private String requestUrl;
	
	
	private String clientUrl;
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
	public String getRequestUrl() {
		return requestUrl;
	}
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}
	public String getClientUrl() {
		return clientUrl;
	}
	public void setClientUrl(String clientUrl) {
		this.clientUrl = clientUrl;
	}
	}