package com.etao.restaurantentity.servicedomain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EtsPayment implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String partnerNo;
	
	private String paymentTypeCode;
	
	private String businessPlatformCode;
	
	private EtsPaymentOrder order;
	public String getPartnerNo() {
		return partnerNo;
	}
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}
	public String getPaymentTypeCode() {
		return paymentTypeCode;
	}
	public void setPaymentTypeCode(String paymentTypeCode) {
		this.paymentTypeCode = paymentTypeCode;
	}
	public String getBusinessPlatformCode() {
		return businessPlatformCode;
	}
	public void setBusinessPlatformCode(String businessPlatformCode) {
		this.businessPlatformCode = businessPlatformCode;
	}
	public EtsPaymentOrder getOrder() {
		return order;
	}
	public void setOrder(EtsPaymentOrder order) {
		this.order = order;
	}
}