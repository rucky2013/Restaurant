package com.etao.restaurantentity.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiMessage {
	
	public ApiMessage()
    {
        this.statusCode = 200;
        this.message="操作成功";
    }
	@JsonProperty(value="StatusCode")	
	private int statusCode;
	@JsonProperty(value = "Message")
	private String message;

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}