package com.etao.restaurantentity.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Response<T> extends ApiResponse {
	@JsonProperty(value = "Result")
	private T result;

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}
}
