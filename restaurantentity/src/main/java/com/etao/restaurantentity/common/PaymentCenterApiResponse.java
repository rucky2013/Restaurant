package com.etao.restaurantentity.common;


import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentCenterApiResponse<T>{
	@JsonProperty(value = "Result")
	private T result;
	
	@JsonProperty(value = "MessageCode")
	private String messageCode;
	@JsonProperty(value = "Message")
	private String message;

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}