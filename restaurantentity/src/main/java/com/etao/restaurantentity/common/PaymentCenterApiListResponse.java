package com.etao.restaurantentity.common;


import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentCenterApiListResponse<T>{
	@JsonProperty(value = "Result")
	private List<T> result;
	
	@JsonProperty(value = "MessageCode")
	private String messageCode;
	@JsonProperty(value = "Message")
	private String message;

	public List<T> getResult() {
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}