package com.etao.restaurantentity.common;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ListResponse<T> extends ApiResponse {
	@JsonProperty(value = "Result")
	private List<T> result;

	public List<T> getResult() {
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}
	
}