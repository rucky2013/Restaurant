package com.etao.restaurantentity.resp;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SupplierDishModel{
	@JsonProperty(value = "Price")
	private BigDecimal price;
	@JsonProperty(value = "ImagePath")
	private String imagePath;
	@JsonProperty(value = "SupplierDishId")
	private int supplierDishId;
	@JsonProperty(value = "SupplierDishName")
	private String supplierDishName;
	@JsonProperty(value = "SuppllierDishDescription")
	private String suppllierDishDescription;
	@JsonProperty(value = "AverageRating")
	private int averageRating;
	@JsonProperty(value = "IsCommission")
	private boolean isCommission;
	@JsonProperty(value = "IsDiscount")
	private boolean isDiscount;
	@JsonProperty(value = "Recipe")
	private String recipe;
	@JsonProperty(value = "Recommended")
	private boolean recommended;
	@JsonProperty(value = "PackagingFee")
	private BigDecimal packagingFee;
	@JsonProperty(value = "Type")
	private int type;
	@JsonProperty(value = "SupplierDishOptionGroup")
	private String supplierDishOptionGroup;
	@JsonProperty(value = "SupplierCatogryId")
	private int supplierCatogryId;
	@JsonProperty(value = "SupplierMenuCategoryId")
	private int supplierMenuCategoryId;
	@JsonProperty(value = "DishCount")
	private int dishCount;
	
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public int getSupplierDishId() {
		return supplierDishId;
	}
	public void setSupplierDishId(int supplierDishId) {
		this.supplierDishId = supplierDishId;
	}
	public String getSupplierDishName() {
		return supplierDishName;
	}
	public void setSupplierDishName(String supplierDishName) {
		this.supplierDishName = supplierDishName;
	}
	public String getSuppllierDishDescription() {
		return suppllierDishDescription;
	}
	public void setSuppllierDishDescription(String suppllierDishDescription) {
		this.suppllierDishDescription = suppllierDishDescription;
	}
	public int getAverageRating() {
		return averageRating;
	}
	public void setAverageRating(int averageRating) {
		this.averageRating = averageRating;
	}
	public boolean getIsCommission() {
		return isCommission;
	}
	public void setIsCommission(boolean isCommission) {
		this.isCommission = isCommission;
	}
	public boolean getIsDiscount() {
		return isDiscount;
	}
	public void setIsDiscount(boolean isDiscount) {
		this.isDiscount = isDiscount;
	}
	public String getRecipe() {
		return recipe;
	}
	public void setRecipe(String recipe) {
		this.recipe = recipe;
	}
	public boolean getRecommended() {
		return recommended;
	}
	public void setRecommended(boolean recommended) {
		this.recommended = recommended;
	}
	public BigDecimal getPackagingFee() {
		return packagingFee;
	}
	public void setPackagingFee(BigDecimal packagingFee) {
		this.packagingFee = packagingFee;
	}
	public String getSupplierDishOptionGroup() {
		return supplierDishOptionGroup;
	}
	public void setSupplierDishOptionGroup(String supplierDishOptionGroup) {
		this.supplierDishOptionGroup = supplierDishOptionGroup;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getSupplierMenuCategoryId() {
		return supplierMenuCategoryId;
	}
	public void setSupplierMenuCategoryId(int supplierMenuCategoryId) {
		this.supplierMenuCategoryId = supplierMenuCategoryId;
	}
	public int getDishCount() {
		return dishCount;
	}
	public void setDishCount(int dishCount) {
		this.dishCount = dishCount;
	}
	public int getSupplierCatogryId() {
		return supplierCatogryId;
	}
	public void setSupplierCatogryId(int supplierCatogryId) {
		this.supplierCatogryId = supplierCatogryId;
	}
}
