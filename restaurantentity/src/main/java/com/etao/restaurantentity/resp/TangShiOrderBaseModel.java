package com.etao.restaurantentity.resp;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TangShiOrderBaseModel {
	@JsonProperty(value = "OrderNumber")
	private String orderNumber;//订单号
	@JsonProperty(value = "DateReserved")
	private Date dateReserved;//下单时间
	@JsonProperty(value = "PaymentId")
	private int paymentId;//支付方式ID
	@JsonProperty(value = "TableStatus")
	private int tableStatus;//订单状态
	@JsonProperty(value = "CustomerTotal")
	private BigDecimal customerTotal;//应收用户金额
	@JsonProperty(value = "IsConfirm")
	private boolean isConfirm;//接口需要，原逻辑为判断有支付流水则为true。此处为True
	@JsonProperty(value = "IsPaId")
	private boolean isPaId;//是否已支付
	@JsonProperty(value = "DeviceNumber")
	private String deviceNumber;//设备号，实际为ContactName列
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public Date getDateReserved() {
		return dateReserved;
	}
	public void setDateReserved(Date dateReserved) {
		this.dateReserved = dateReserved;
	}
	
	public int getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	
	public int getTableStatus() {
		return tableStatus;
	}
	public void setTableStatus(int tableStatus) {
		this.tableStatus = tableStatus;
	}
	
	public BigDecimal getCustomerTotal() {
		return customerTotal;
	}
	public void setCustomerTotal(BigDecimal customerTotal) {
		this.customerTotal = customerTotal;
	}
	
	public boolean getIsConfirm() {
		return isConfirm;
	}
	public void setIsConfirm(boolean isConfirm) {
		this.isConfirm = isConfirm;
	}
	
	public boolean getIsPaId() {
		return isPaId;
	}
	public void setIsPaId(boolean isPaId) {
		this.isPaId = isPaId;
	}
	
	public String getDeviceNumber() {
		return deviceNumber;
	}
	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}
	
}
