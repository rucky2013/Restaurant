package com.etao.restaurantentity.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SaveTangShiOrderResp{
	@JsonProperty(value = "OrderId")
	private String orderId;
	@JsonProperty(value = "Qr")
	private String qr;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getQr() {
		return qr;
	}
	public void setQr(String qr) {
		this.qr = qr;
	}
}