package com.etao.restaurantentity.resp;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SupplierDishResp{
	@JsonProperty(value = "CategoryId")
	private int categoryId;
	@JsonProperty(value = "CategoryName")
	private String categoryName;
	@JsonProperty(value = "SupplierDishList")
	private List<SupplierDishModel> supplierDishList;
	public List<SupplierDishModel> getSupplierDishList() {
		return supplierDishList;
	}
	public void setSupplierDishList(List<SupplierDishModel> supplierDishList) {
		this.supplierDishList = supplierDishList;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
}
