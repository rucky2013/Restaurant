package com.etao.restaurantentity.resp;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderQueryResult {
	

	@JsonProperty(value = "PartnerNo")
	private String partnerNo;
	@JsonProperty(value = "PaymentTypeCode")
	private String paymentTypeCode;
	@JsonProperty(value = "Amount")
	private String amount;
	@JsonProperty(value = "OrderId") 
	private String orderId;
	@JsonProperty(value = "PayTime")
	private String payTime;
	@JsonProperty(value = "PaymentNo")
	private String paymentNo;
	@JsonProperty(value = "PaymentStatus")
	private int paymentStatus;
	@JsonProperty(value = "Message")
	private String message;
	@JsonProperty(value = "BuyerIdOrOpenid")
	private String buyerIdOrOpenid;
	@JsonProperty(value = "PaymentFlowNo")
	private String paymentFlowNo;
	@JsonProperty(value = "AppID")
	private String appID;
	
	public String getPartnerNo() {
		return partnerNo;
	}
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}
	public String getPaymentTypeCode() {
		return paymentTypeCode;
	}
	public void setPaymentTypeCode(String paymentTypeCode) {
		this.paymentTypeCode = paymentTypeCode;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPayTime() {
		return payTime;
	}
	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}
	public String getPaymentNo() {
		return paymentNo;
	}
	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}
	public int getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getBuyerIdOrOpenid() {
		return buyerIdOrOpenid;
	}
	public void setBuyerIdOrOpenid(String buyerIdOrOpenid) {
		this.buyerIdOrOpenid = buyerIdOrOpenid;
	}
	public String getPaymentFlowNo() {
		return paymentFlowNo;
	}
	public void setPaymentFlowNo(String paymentFlowNo) {
		this.paymentFlowNo = paymentFlowNo;
	}
	public String getAppID() {
		return appID;
	}
	public void setAppID(String appID) {
		this.appID = appID;
	}
}
