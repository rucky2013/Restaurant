package com.etao.restaurantentity.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.etao.restaurantentity.common.QueryModel;

/**
 * 堂食订单表实体
 * @author xupeng
 * @Time 2016-03-23
 */
public class DineEntity extends QueryModel implements Serializable {
	private static final long serialVersionUID = 1L;
	public void clear() {
		super.clear();		
	
		dineId=0;
		orderNumber=null;
		creatTime=null;
		deskNo=null;
		customerId=0;
		contactName=null;
		contactPhone=null;
		contactGender=0;
		orderStatusId=0;
		supplierId=0;
		orderTotal=null;
		customerTotal=null;
		couponTotal=null;
		serviceFee=null;
		invoiceRequired=false;
	}

	private int dineId;//ID 
	private String orderNumber;//订单编号 
	private Date creatTime;//订单创建时间 
	private String deskNo;//桌号 
	private int customerId;//客户ID 
	private String contactName;//联系人 
	private String contactPhone;//联系电话
	private int contactGender;//联系人性别 
	private int orderStatusId;//订单状态 
	private int supplierId;//商户（门店）ID 
	private BigDecimal orderTotal; //总价
	private BigDecimal customerTotal;//应收用户金额
	private BigDecimal couponTotal;//优惠金额
	private BigDecimal serviceFee;//服务费 
	private boolean invoiceRequired;//是否开发票
	private String invoiceTitle;//发票抬头
	private int paymentMethodId;//支付方式Id
	private boolean isPaid;//是否已支付
	private String customerNotes;//用户备注
	private String serverNotes;//客服备注
	private Date modifyTime;//修改时间
	private String templateid;//平台（Wap,Android....)
	private String path;//用户途径(meizhou,....)
	private int fromType; //来源业务类型（0：默认；1？，订台；2？，排队）
	private String dishNames;//菜品名称
	private boolean isAlerted;//是否已经提醒（CC使用）
	private Date paymentDate;//支付时间
	private int promotionType;//促销类型（NULL：使用Admin区分平台的促销方式；1：使用Admin区分平台的促销方式；2：使用CRM促销方式）
	private String tradeId;//
	private String tradeGuid; 
	private String clientId; 
	private String partnerId; 
	private String transactionCode; 
	private int customerCount; 
	private int waitStaffId;//服务员ID
	private int qualifiedOrder; 
	private int deskId;//桌号
	private String mealCode;//取餐码
	private String paymentTypeCode;
	
	public int getDineId() {
		return dineId;
	}
	public void setDineId(int dineId) {
		this.dineId = dineId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Date getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}

	public String getDeskNo() {
		return deskNo;
	}

	public void setDeskNo(String deskNo) {
		this.deskNo = deskNo;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public int getContactGender() {
		return contactGender;
	}

	public void setContactGender(int contactGender) {
		this.contactGender = contactGender;
	}

	public int getOrderStatusId() {
		return orderStatusId;
	}

	public void setOrderStatusId(int orderStatusId) {
		this.orderStatusId = orderStatusId;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public BigDecimal getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(BigDecimal orderTotal) {
		this.orderTotal = orderTotal;
	}

	public BigDecimal getCustomerTotal() {
		return customerTotal;
	}

	public void setCustomerTotal(BigDecimal customerTotal) {
		this.customerTotal = customerTotal;
	}

	public BigDecimal getCouponTotal() {
		return couponTotal;
	}

	public void setCouponTotal(BigDecimal couponTotal) {
		this.couponTotal = couponTotal;
	}

	public BigDecimal getServiceFee() {
		return serviceFee;
	}

	public void setServiceFee(BigDecimal serviceFee) {
		this.serviceFee = serviceFee;
	}

	public boolean getInvoiceRequired() {
		return invoiceRequired;
	}

	public void setInvoiceRequired(boolean invoiceRequired) {
		this.invoiceRequired = invoiceRequired;
	}

	public String getInvoiceTitle() {
		return invoiceTitle;
	}

	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}

	public int getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(int paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}

	public String getCustomerNotes() {
		return customerNotes;
	}

	public void setCustomerNotes(String customerNotes) {
		this.customerNotes = customerNotes;
	}

	public String getServerNotes() {
		return serverNotes;
	}

	public void setServerNotes(String serverNotes) {
		this.serverNotes = serverNotes;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getTemplateid() {
		return templateid;
	}

	public void setTemplateid(String templateid) {
		this.templateid = templateid;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getFromType() {
		return fromType;
	}

	public void setFromType(int fromType) {
		this.fromType = fromType;
	}

	public String getDishNames() {
		return dishNames;
	}

	public void setDishNames(String dishNames) {
		this.dishNames = dishNames;
	}

	public boolean getIsAlerted() {
		return isAlerted;
	}

	public void setIsAlerted(boolean isAlerted) {
		this.isAlerted = isAlerted;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public int getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(int promotionType) {
		this.promotionType = promotionType;
	}

	public String getTradeId() {
		return tradeId;
	}

	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	public String getTradeGuid() {
		return tradeGuid;
	}

	public void setTradeGuid(String tradeGuid) {
		this.tradeGuid = tradeGuid;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public int getCustomerCount() {
		return customerCount;
	}

	public void setCustomerCount(int customerCount) {
		this.customerCount = customerCount;
	}

	public int getWaitStaffId() {
		return waitStaffId;
	}

	public void setWaitStaffId(int waitStaffId) {
		this.waitStaffId = waitStaffId;
	}

	public int getQualifiedOrder() {
		return qualifiedOrder;
	}

	public void setQualifiedOrder(int qualifiedOrder) {
		this.qualifiedOrder = qualifiedOrder;
	}
	
	public int getDeskId() {
		return deskId;
	}
	public void setDeskId(int deskId) {
		this.deskId = deskId;
	}
	
	public String getMealCode() {
		return mealCode;
	}
	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}
	public String getPaymentTypeCode() {
		return paymentTypeCode;
	}
	public void setPaymentTypeCode(String paymentTypeCode) {
		this.paymentTypeCode = paymentTypeCode;
	}

	
}
