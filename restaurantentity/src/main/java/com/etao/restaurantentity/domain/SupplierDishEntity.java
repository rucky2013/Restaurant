package com.etao.restaurantentity.domain;



import java.io.Serializable;
import java.math.BigDecimal;

import com.etao.restaurantentity.common.QueryModel;



/**
 * 菜品对象
 * @author xuyulong
 *
 */
public class SupplierDishEntity extends QueryModel implements Serializable {
	private static final long serialVersionUID = 1L;
	public void clear() {
		super.clear();
		supplierDishID=0;
		supplierID=0;
		supplierCategoryID=0;
		dishImageID=0;
		dishID=0;
		dishNo=null;
		supplierDishName=null;
		price=null;
	}
	private int supplierDishID;
	private int supplierID;
	private int supplierCategoryID;
	private int dishImageID;
	private int dishID;
	private String dishNo;
	private String supplierDishName;
	private BigDecimal price;
	private String suppllierDishDescription;
	private int averageRating;
	private boolean isCommission;
	private boolean isDiscount;
	private String recipe;
	private boolean recommended;
	private BigDecimal packagingFee;
	
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getSupplierDishName() {
		return supplierDishName;
	}
	public void setSupplierDishName(String supplierDishName) {
		this.supplierDishName = supplierDishName;
	}
	public String getDishNo() {
		return dishNo;
	}
	public void setDishNo(String dishNo) {
		this.dishNo = dishNo;
	}
	public int getDishID() {
		return dishID;
	}
	public void setDishID(int dishID) {
		this.dishID = dishID;
	}
	public int getDishImageID() {
		return dishImageID;
	}
	public void setDishImageID(int dishImageID) {
		this.dishImageID = dishImageID;
	}
	public int getSupplierCategoryID() {
		return supplierCategoryID;
	}
	public void setSupplierCategoryID(int supplierCategoryID) {
		this.supplierCategoryID = supplierCategoryID;
	}
	public int getSupplierID() {
		return supplierID;
	}
	public void setSupplierID(int supplierID) {
		this.supplierID = supplierID;
	}
	public int getSupplierDishID() {
		return supplierDishID;
	}
	public void setSupplierDishID(int supplierDishID) {
		this.supplierDishID = supplierDishID;
	}
	public String getSuppllierDishDescription() {
		return suppllierDishDescription;
	}
	public void setSuppllierDishDescription(String suppllierDishDescription) {
		this.suppllierDishDescription = suppllierDishDescription;
	}
	public int getAverageRating() {
		return averageRating;
	}
	public void setAverageRating(int averageRating) {
		this.averageRating = averageRating;
	}
	public boolean getIsCommission() {
		return isCommission;
	}
	public void setIsCommission(boolean isCommission) {
		this.isCommission = isCommission;
	}
	public BigDecimal getPackagingFee() {
		return packagingFee;
	}
	public void setPackagingFee(BigDecimal packagingFee) {
		this.packagingFee = packagingFee;
	}
	public boolean getRecommended() {
		return recommended;
	}
	public void setRecommended(boolean recommended) {
		this.recommended = recommended;
	}
	public String getRecipe() {
		return recipe;
	}
	public void setRecipe(String recipe) {
		this.recipe = recipe;
	}
	public boolean getIsDiscount() {
		return isDiscount;
	}
	public void setIsDiscount(boolean isDiscount) {
		this.isDiscount = isDiscount;
	}
}