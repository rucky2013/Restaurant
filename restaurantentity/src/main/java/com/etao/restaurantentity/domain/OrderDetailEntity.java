package com.etao.restaurantentity.domain;



import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.etao.restaurantentity.common.QueryModel;



/**
 * 堂食明细对象
 * @author xuyulong
 *
 */
public class OrderDetailEntity extends QueryModel implements Serializable {
	private static final long serialVersionUID = 1L;
	public void clear() {
		super.clear();
		/*supplierDishID=0;
		supplierID=0;
		supplierCategoryID=0;
		dishImageID=0;
		dishID=0;
		dishNo=null;
		supplierDishName=null;
		price=null;*/
	}
	public int getOrderDetailId() {
		return orderDetailId;
	}
	public void setOrderDetailId(int orderDetailId) {
		this.orderDetailId = orderDetailId;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getOrderType() {
		return orderType;
	}
	public void setOrderType(int orderType) {
		this.orderType = orderType;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getSupplierDishId() {
		return supplierDishId;
	}
	public void setSupplierDishId(int supplierDishId) {
		this.supplierDishId = supplierDishId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getSupplierPrice() {
		return supplierPrice;
	}
	public void setSupplierPrice(BigDecimal supplierPrice) {
		this.supplierPrice = supplierPrice;
	}
	public String getSpecialInstruction() {
		return specialInstruction;
	}
	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getSupplierDishName() {
		return supplierDishName;
	}
	public void setSupplierDishName(String supplierDishName) {
		this.supplierDishName = supplierDishName;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public boolean getIsAdd() {
		return isAdd;
	}
	public void setIsAdd(boolean isAdd) {
		this.isAdd = isAdd;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	private int orderDetailId;
	private int orderId;
	private int orderType;
	private int customerId;
	private int supplierDishId;
	private int quantity;
	private BigDecimal supplierPrice;
	private BigDecimal totalAmount;
	private String specialInstruction;
	private Date orderDate;
	private boolean isAdd;
	private int status;
	private String supplierDishName;
	private String note;
	private String orderNo;
}