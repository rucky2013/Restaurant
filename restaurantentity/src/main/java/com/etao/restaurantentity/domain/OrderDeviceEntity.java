package com.etao.restaurantentity.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.etao.restaurantentity.common.QueryModel;


/**
 * 设备订单表实体
 * @author xupeng
 * @Time 2016-03-23
 */
public class OrderDeviceEntity extends QueryModel implements Serializable {
	private static final long serialVersionUID = 1L;
	public void clear() {
		super.clear();		
	
		orderDeviceId=0;
		orderNumber=0;
		orderDate=null;
		deviceNumber=null;
		isTakePackage=0;
	}
	
	private int orderDeviceId;//ID
	
	private int orderNumber;//订单编号

	private Date orderDate;//订单创建时间 

	private String deviceNumber;//设备编号 

    private int isTakePackage;

    private int seriesNumber;//设备当天累计单量

	public int getOrderDeviceId() {
		return orderDeviceId;
	}
	public void setOrderDeviceId(int orderDeviceId) {
		this.orderDeviceId = orderDeviceId;
	}
	
	public int getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getDeviceNumber() {
		return deviceNumber;
	}
	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}
	
	public int getIsTakePackage() {
		return isTakePackage;
	}
	public void setIsTakePackage(int isTakePackage) {
		this.isTakePackage = isTakePackage;
	}

	public int getSeriesNumber() {
		return seriesNumber;
	}
	public void setSeriesNumber(int seriesNumber) {
		this.seriesNumber = seriesNumber;
	}
}
