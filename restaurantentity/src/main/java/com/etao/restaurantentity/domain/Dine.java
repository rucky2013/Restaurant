package com.etao.restaurantentity.domain;

import java.io.Serializable;
import java.util.Date;

import com.etao.restaurantentity.common.QueryModel;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Dine extends QueryModel implements Serializable{
	private static final long serialVersionUID = 1L;
	private int dineId;	
	private String orderNumber;
	private Date creatTime;
	private String deskNo;
	private int customerID;
	private String contactName;
	private String contactPhone;
	private int contactGender;
	private int orderStatusId;
	private int supplierID;
	private double totalAmount;
	private int customerCount;
	private double customerTotal;
	private double couponTotal;
	private double serviceFee;
	private boolean invoiceRequired;
	private String invoiceTitle;
	private int paymentMethodId;
	private boolean isPaid;
	private String customerNotes;
	private String serverNotes;
	private Date modifyTime;
	private String templateid;
	private String path;
	private int fromType;
	private String dishNames;
	private Date paymentDate;
	private String partnerID;
	private boolean isAlerted;
	private int deskId;
	private int promotionType;
	private int qualifiedOrder;
	public int getDineId() {
		return dineId;
	}
	public void setDineId(int dineId) {
		this.dineId = dineId;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Date getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}
	public String getDeskNo() {
		return deskNo;
	}
	public void setDeskNo(String deskNo) {
		this.deskNo = deskNo;
	}
	public int getCustomerID() {
		return customerID;
	}
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public int getContactGender() {
		return contactGender;
	}
	public void setContactGender(int contactGender) {
		this.contactGender = contactGender;
	}
	public int getOrderStatusId() {
		return orderStatusId;
	}
	public void setOrderStatusId(int orderStatusId) {
		this.orderStatusId = orderStatusId;
	}
	public int getSupplierID() {
		return supplierID;
	}
	public void setSupplierID(int supplierID) {
		this.supplierID = supplierID;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public int getCustomerCount() {
		return customerCount;
	}
	public void setCustomerCount(int customerCount) {
		this.customerCount = customerCount;
	}
	public double getCustomerTotal() {
		return customerTotal;
	}
	public void setCustomerTotal(double customerTotal) {
		this.customerTotal = customerTotal;
	}
	public double getCouponTotal() {
		return couponTotal;
	}
	public void setCouponTotal(double couponTotal) {
		this.couponTotal = couponTotal;
	}
	public double getServiceFee() {
		return serviceFee;
	}
	public void setServiceFee(double serviceFee) {
		this.serviceFee = serviceFee;
	}
	public boolean isInvoiceRequired() {
		return invoiceRequired;
	}
	public void setInvoiceRequired(boolean invoiceRequired) {
		this.invoiceRequired = invoiceRequired;
	}
	public String getInvoiceTitle() {
		return invoiceTitle;
	}
	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}
	public int getPaymentMethodId() {
		return paymentMethodId;
	}
	public void setPaymentMethodId(int paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}
	public boolean getIsPaid() {
		return isPaid;
	}
	public void setIsPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}
	public String getCustomerNotes() {
		return customerNotes;
	}
	public void setCustomerNotes(String customerNotes) {
		this.customerNotes = customerNotes;
	}
	public String getServerNotes() {
		return serverNotes;
	}
	public void setServerNotes(String serverNotes) {
		this.serverNotes = serverNotes;
	}
	public Date getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
	public String getTemplateid() {
		return templateid;
	}
	public void setTemplateid(String templateid) {
		this.templateid = templateid;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getFromType() {
		return fromType;
	}
	public void setFromType(int fromType) {
		this.fromType = fromType;
	}
	public String getDishNames() {
		return dishNames;
	}
	public void setDishNames(String dishNames) {
		this.dishNames = dishNames;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getPartnerID() {
		return partnerID;
	}
	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}
	public boolean getIsAlerted() {
		return isAlerted;
	}
	public void setIsAlerted(boolean isAlerted) {
		this.isAlerted = isAlerted;
	}
	public int getDeskId() {
		return deskId;
	}
	public void setDeskId(int deskId) {
		this.deskId = deskId;
	}
	public int getPromotionType() {
		return promotionType;
	}
	public void setPromotionType(int promotionType) {
		this.promotionType = promotionType;
	}
	public int getQualifiedOrder() {
		return qualifiedOrder;
	}
	public void setQualifiedOrder(int qualifiedOrder) {
		this.qualifiedOrder = qualifiedOrder;
	}
}