package com.etao.restaurantservice.service.impl;

/**
 * 2012-7-8
 * jqsl2012@163.com
 */


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etao.restaurantdao.dao.impl.SupplierDishDaoImpl;
import com.etao.restaurantdao.dao.inter.ISupplierDishDao;
import com.etao.restaurantentity.servicedomain.SupplierDish;
import com.etao.restaurantservice.common.ServersManager;
import com.etao.restaurantservice.service.inter.ISupplierDishService;

import javax.annotation.Resource;


/**
 * @author huangf
 */
@Service
public class SupplierDishServiceImpl extends ServersManager<SupplierDish, ISupplierDishDao> implements
ISupplierDishService {
	
	/*@Resource(name="supplierDishDaoImpl")*/
	/*@Override
    public void setDao(ISupplierDishDao supplierDishDao) {
        this.dao = supplierDishDao;
    }*/

	
	@Override
	public List<SupplierDish> selectSupplierDishMenu(SupplierDish e)
	{
		return dao.selectSupplierDishMenu(e);
	}

}
