package com.etao.restaurantservice.service.inter;

import java.util.List;

import com.etao.restaurantentity.servicedomain.SupplierDish;
import com.etao.restaurantservice.common.Services;

public interface ISupplierDishService extends Services<SupplierDish> {
	
	List<SupplierDish> selectSupplierDishMenu(SupplierDish e);
}
