package com.etao.restaurantservice.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * 2016.03.24
 * xu.yulong@etao.cn
 */


import java.util.List;

import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import com.etao.restaurantdao.dao.impl.SupplierDishDaoImpl;
import com.etao.restaurantdao.dao.inter.IDineDao;
import com.etao.restaurantdao.dao.inter.IOrderDetailDao;
import com.etao.restaurantdao.dao.inter.ISupplierDishDao;
import com.etao.restaurantentity.req.SupplierDishItem;
import com.etao.restaurantentity.servicedomain.Dine;
import com.etao.restaurantentity.servicedomain.OrderDetail;
import com.etao.restaurantentity.servicedomain.SupplierDish;
import com.etao.restaurantservice.common.ServersManager;
import com.etao.restaurantservice.service.inter.IDineService;
import com.etao.restaurantservice.service.inter.ISupplierDishService;

import javax.annotation.Resource;
import javax.swing.JSpinner.DateEditor;


/**
 * @author xuyulong
 */
@Service
public class DineServiceImpl extends ServersManager<Dine, IDineDao> implements
IDineService {

	@Autowired
	private IOrderDetailDao orderDetailDao;
	
	/**
	 * @author xuyulong
	 * app实现堂食下单
	 * @throws ParseException 
	 */
	public Dine saveTangShiOrder(Dine dine) throws ParseException
	{
		// TODO 实现堂食订单的业务逻辑
		dine.setContactPhone("");
		//dine.setCreatTime(new Date());
		Date nowDate=new Date();
		dine.setCreatTime(nowDate);
		dine.setCustomerId(0);
		dine.setOrderStatusId(1);
		dine.setIsPaid(false);
		String StringDate = "1900-01-01";
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = sdf.parse(StringDate);
		dine.setModifyTime(date);
		dine.setFromType(1);
		dine.setQualifiedOrder(0);
		//获取堂食订单编号
		String orderNumber=dao.GetDineNumber(dine);
		dine.setOrderNumber(orderNumber);
		//插入堂食订单
		int row=dao.insert(dine);
		List<OrderDetail> orderDetails=new ArrayList<OrderDetail>();
		for (SupplierDishItem item : dine.getSupplierDishList()) {
			OrderDetail e=new OrderDetail();
			e.setSupplierDishName(item.getSupplierDishName());
			e.setSupplierDishId(item.getSupplierDishId());
			e.setOrderId(dine.getId());
			e.setOrderNo(orderNumber);
			e.setQuantity(item.getDishCount());
			e.setOrderDate(nowDate);
			e.setCustomerId(0);
			java.text.DecimalFormat   df   =new   java.text.DecimalFormat("#.00");  
			e.setSupplierPrice(new BigDecimal(df.format(item.getPrice())));
			e.setTotalAmount(new BigDecimal(item.getDishCount()).multiply(new BigDecimal(df.format(item.getPrice()))));
			orderDetails.add(e);
		}
		orderDetailDao.insertList(orderDetails);
		return dine;
	}
	
	public int updateIsPaid(Dine e) {
		return dao.updateIsPaid(e);
	}

}
