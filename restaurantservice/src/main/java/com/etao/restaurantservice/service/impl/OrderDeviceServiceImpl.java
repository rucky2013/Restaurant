package com.etao.restaurantservice.service.impl;

import org.springframework.stereotype.Service;

import com.etao.restaurantdao.dao.inter.IOrderDeviceDao;
import com.etao.restaurantentity.servicedomain.OrderDevice;
import com.etao.restaurantservice.common.ServersManager;
import com.etao.restaurantservice.service.inter.IOrderDeviceService;

@Service
public class OrderDeviceServiceImpl extends ServersManager<OrderDevice, IOrderDeviceDao> implements
IOrderDeviceService{

}
