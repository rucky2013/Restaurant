package com.etao.restaurantservice.service.inter;

import java.text.ParseException;

import com.etao.restaurantentity.servicedomain.Dine;
import com.etao.restaurantservice.common.Services;

public interface IDineService extends Services<Dine> {
	
	Dine saveTangShiOrder(Dine dine) throws ParseException;
	int updateIsPaid(Dine e);
}
