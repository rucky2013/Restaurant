package com.etao.restaurantservice.service.inter;

import com.etao.restaurantentity.servicedomain.OrderDevice;
import com.etao.restaurantservice.common.Services;

public interface IOrderDeviceService extends Services<OrderDevice>{
}
